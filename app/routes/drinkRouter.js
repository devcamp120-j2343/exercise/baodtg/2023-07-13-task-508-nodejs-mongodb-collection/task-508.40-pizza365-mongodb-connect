//Khai báo thư viện express
const express = require('express');

//Tạo router:
const drinkRouter = express.Router();

//router get all drinks
drinkRouter.get("/drinks", (req, res) => {
    console.log(req.method);
    console.log("Get all drinks");
    res.json({
        message: `Get all drinks`
    })
})

//router get drink
drinkRouter.get("/drinks/:drinkId", (req, res) => {
    let drinkId = req.params.drinkId
    console.log(req.method);

    console.log("Get drink");
    res.json({
        message: `Get drink with id: ${drinkId}`
    })
})

//router post drinks
drinkRouter.post("/drinks", (req, res) => {
    console.log(req.method);
    console.log("Create new drink");
    res.json({
        message: `Create new drink`
    })
})

//router put drink
drinkRouter.put("/drinks/:drinkId", (req, res) => {
    console.log(req.method);
    let drinkId = req.params.drinkId
    console.log("Update drink");
    res.json({
        message: `Update drink with id: ${drinkId}`
    })
})

//router delete drink
drinkRouter.delete("/drinks/:drinkId", (req, res) => {
    console.log(req.method);
    let drinkId = req.params.drinkId
    console.log("Delete drink");
    res.json({
        message: `Delete drinks with id: ${drinkId}`
    })
})

module.exports = {drinkRouter}

