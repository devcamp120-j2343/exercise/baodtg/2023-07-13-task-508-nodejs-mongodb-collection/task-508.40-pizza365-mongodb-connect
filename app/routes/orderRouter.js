//Khai báo thư viện express
const express = require('express')

//Tạo router
const orderRouter = express.Router();

//Router get all order:
orderRouter.get('/orders', (req, res) => {
    console.log(req.method);
    console.log(`Get all orders`);
    res.json({
        message: `Get all orders`
    })
})

//Router get order by id:
orderRouter.get('/orders/:orderId', (req, res) =>{
    const orderId = req.params.orderId;
    console.log(req.method);
    console.log(`Get order by Id: ${orderId}`);
    res.json({
        message: `Get order by Id: ${orderId}`
    })
})

//Router post order(create new order)
orderRouter.post('/orders', (req, res) => {
    console.log(req.method);
    console.log(`Create new order`);
    res.json({
        message: `Create new order`
    });
})

//Router put order (update order by id)
orderRouter.put('/orders/:orderId', (req, res) => {
    const orderId = req.params.orderId;
    console.log(req.method);
    console.log(`Update order by Id`);
    res.json({
        message: `Update order by Id: ${orderId}`
    });
})

//Router delete order (delete order by Id):
orderRouter.delete('/orders/:orderId', (req, res) => {
    const orderId = req.params.orderId;
    console.log(req.method);
    console.log(`Delete order by Id`);
    res.json({
        message: `Delete order by Id: ${orderId}`
    })
})

module.exports = {orderRouter}