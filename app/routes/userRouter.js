//Khai báo thư viện express
const express = require('express')

//Tạo router
const userRouter = express.Router();

//Router get all user:
userRouter.get('/users', (req, res) => {
    console.log(req.method);
    console.log(`Get all users`);
    res.json({
        message: `Get all users`
    })
})

//Router get user by id:
userRouter.get('/users/:userId', (req, res) =>{
    const userId = req.params.userId;
    console.log(req.method);
    console.log(`Get user by Id: ${userId}`);
    res.json({
        message: `Get user by Id: ${userId}`
    })
})

//Router post user(create new user)
userRouter.post('/users', (req, res) => {
    console.log(req.method);
    console.log(`Create new user`);
    res.json({
        message: `Create new user`
    });
})

//Router put user (update user by id)
userRouter.put('/users/:userId', (req, res) => {
    const userId = req.params.userId;
    console.log(req.method);
    console.log(`Update user by Id`);
    res.json({
        message: `Update user by Id: ${userId}`
    });
})

//Router delete user (delete user by Id):
userRouter.delete('/users/:userId', (req, res) => {
    const userId = req.params.userId;
    console.log(req.method);
    console.log(`Delete user by Id`);
    res.json({
        message: `Delete user by Id: ${userId}`
    })
})

module.exports = {userRouter}