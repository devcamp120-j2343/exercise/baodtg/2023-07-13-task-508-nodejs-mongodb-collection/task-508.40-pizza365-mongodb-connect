//B1: khai báo thư viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema cua mongoose:
const Schema = mongoose.Schema;

//B3: tạo đối tượng Schema bao gồm các thuộc tính trong mongoDB:
const userSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    address: {
        type: String,
        required: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    orders: {
        type: mongoose.Types.ObjectId,
        ref: "order"
    }
})

//B4: export schema ra model:
module.exports = mongoose.model('user', userSchema)