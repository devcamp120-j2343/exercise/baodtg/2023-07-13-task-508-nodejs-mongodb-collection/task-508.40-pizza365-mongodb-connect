//B1: Khai báo thu viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thu viện Schema của mongoose:
const Schema = mongoose.Schema;

//B3: Tạo đối tượng Schema bao gồm các thuộc tính trong mongoDB:
const voucherSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        unique: true,
        required: true
    },
    phanTramGiamGia: {
        type: Number,
        required: true
    },
    ghiChu: {
        type: String,
        required: false
    }
})

//B4: export schema ra model:
module.exports = mongoose.model('voucher', voucherSchema)